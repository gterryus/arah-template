# About

Template for ARAH apps by XAI Syndicate

# Contributor

- Terryus Gunawan | terryusgunawan@gmail.com | [@terryusgunawan](https://www.instagram.com/terryusgunawan/)

# Mockup

https://www.figma.com/file/hoBVueKV3zBN8fmSHghApD/Main_File?node-id=0%3A132