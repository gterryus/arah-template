/* Global letiable */
var window_height;
var window_width;

/* Get window width & height */
function getHeightWidth() {
  /* Set height & width of user-end windows to letiable */
  window_height = $(window).height();
  window_width = $(window).width();
}

/* Scroll to top of window */
function scrollToTop(time_counter){
    $("body,html,document").animate(
        {
        scrollTop: 0
        },
        time_counter
    );
}

function copyToClip(el){
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(el).val()).select();
  document.execCommand("copy");
  $temp.remove();

  alert('Hash code has been copied to your clipboard!')
}

/* This is section onLoad JS script needed */
$(window).on("load", function () {
  scrollToTop(10);
});

/* Toggle sidebar collapsed when resize */
$(window).on("resize", function () {
  // setSidebar();
});

/* This is section document ready JS script needed */
$(function () {
  // Upload image trigger
  $(".upload-image").on('click', function(e){
    e.preventDefault(); 

    let thisEl = $(this);
    let targetEl = thisEl.attr('data-target');

    $(targetEl).trigger('click');
  });

  // Custom select choosen
  $(".custom-choosen-select").on('click', function(e){
    e.preventDefault();
    let thisEl = $(this);

    $(".custom-choosen-select").removeClass('active');
    thisEl.addClass('active');
    thisEl.find('input[type=radio]').prop('checked', true);
    //alert(thisEl.find('input[type=radio]').prop('checked', true).val()); // get value
  });

  // Sidebar toggle
  $('.sidebar-toggle').on('click', function(e){
    e.preventDefault();
    let thisEl = $(this);
    let ariaState = thisEl.attr('aria-expanded');
    let backdrop = `<div class="sidebar-backdrop"></div>`;

    $('body').toggleClass('sidebar-open');
    $('#sidebar').toggleClass('shown');

    if(ariaState === 'false')
      $('.main-content').append(backdrop);
    if(ariaState === 'true')
      $('.sidebar-backdrop').remove();
  });

});
